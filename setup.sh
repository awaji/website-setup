#!/bin/bash

sudo yum install docker -y
sudo service docker start

mkdir /home/ec2-user/bin/
curl -L https://github.com/docker/compose/releases/download/1.13.0/docker-compose-`uname -s`-`uname -m` > /home/ec2-user/bin/docker-compose
sudo chmod +x /home/ec2-user/bin/docker-compose

rm -rf /home/ec2-user/website
rm -rf /home/ec2-user/website.zip

curl -o /home/ec2-user/website.zip
https://s3-us-west-2.amazonaws.com/it26-awaji.morijyobi.ac.jp/website.zip

sleep 5

unzip /home/ec2-user/website.zip -d /home/ec2-user/

cd /home/ec2-user/website

sudo docker info
sudo docker build -t nginx_web01 .
sudo /home/ec2-user/bin/docker-compose up -d