require 'json'
require 'open-uri'

KEY_NAME   = "suzuryo3"
SEC_GROUP  = "sg-e244b998"

puts "setup.rb / start"

running_instances_json = JSON.parse(`aws ec2 describe-instances`)
running_instances_json['Reservations'].each do |i|
  i['Instances'].each do |item|
    running_instance_id = item['InstanceId']
    puts `aws ec2 terminate-instances --instance-ids #{running_instance_id}`
  end
end

instanceid_json = JSON.parse(`aws ec2 run-instances --image-id ami-4836a428 --count 1 --instance-type t2.micro --key-name #{KEY_NAME} --security-group-id #{SEC_GROUP}`)
p instanceid_json

instanceid = instanceid_json['Instances'][0]["InstanceId"]
p instanceid


ipaddress = ""
loop do
  break if ipaddress != "" && ipaddress != nil
  ipaddress_json = JSON.parse(`aws ec2 describe-instances --instance-ids #{instanceid}`)
  p ipaddress_json

  ipaddress = ipaddress_json['Reservations'][0]['Instances'][0]['PublicIpAddress']
  p ipaddress
  sleep 3
end

puts ipaddress

status = ""
loop do
  break if status != "" && status != nil && status == 'ok'
  status_json = JSON.parse(`aws ec2 describe-instance-status --instance-ids #{instanceid}`)
  p status_json

  sleep 3
  status = status_json['InstanceStatuses'][0]['InstanceStatus']['Status'] if status_json['InstanceStatuses'].size != 0
  p status
end

p `ssh -v ec2-user@#{ipaddress} -i /awaji_key.pem "echo hello"`

p `scp /root/setup/setup.sh ec2-user@#{ipaddress}:/home/ec2-user/setup.sh`

p `ssh -v ec2-user@#{ipaddress} "mkdir -p /home/ec2-user/letsencrypt/live/www.awaji.it26.morijyobi.net"`

p `scp /root/setup/privkey.pem ec2-user@#{ipaddress}:/home/ec2-user/letsencrypt/live/www.awaji.it26.morijyobi.net/privkey.pem`

p `scp /root/setup/fullchain.pem ec2-user@#{ipaddress}:/home/ec2-user/letsencrypt/live/www.awaji.it26.morijyobi.net/fullchain.pem`

p `ssh -v ec2-user@#{ipaddress} "/bin/bash /home/ec2-user/setup.sh"`

r53json = <<EOS
{
  "Comment" : "",
  "Changes" : [
    {
      "Action" : "UPSERT",
      "ResourceRecordSet" : {
        "Name" : "www.awaji.it26.morijyobi.net",
        "Type" : "A",
        "TTL" : 60,
        "ResourceRecords" : [
          {
            "Value": "#{ipaddress}"
          }
        ]
      }
    }
  ]
}
EOS

File.open("r53.json","w") do |file|
  file.puts(r53json)
end

p `aws route53 change-resource-record-sets --hosted-zone-id Z1A5I37KC086K7 --change-batch file://r53.json`

url_status = ''
loop do
  break if url_status == '200'
  open("http://www.awaji.it26.morijyobi.net:8080/", read_timeout: 3) do |f|
    p f.status
    url_status = f.status[0]
  end  
  sleep 3
end

puts "setup.rb / finish"

puts "http://#{ipaddress}"
puts "http://www.awaji.it26.morijyobi.net"